<?php
	require "netWf.class.php";

	$netwf = new netWf('http://localhost:9000', '1234567890987654321');

	// Get 5 Rows of Table
	var_dump($netwf->table("table", 5));
	
	// Get Row with id = 2
	var_dump($netwf->tableById("table", "id", 2));
	
	// Get First Row
	var_dump($netwf->first("table", "id", "*"));
	
	// Get Last Row
	var_dump($netwf->last("table", "id", "*"));
	
	// Get Previous Row of 3
	var_dump($netwf->previous("table", "id", 3, "*"));
	
	// Get Next Row of 3
	var_dump($netwf->next("table", "id", 3, "*"));

	// Execute sp_procedure
	$params = array('par1' => 'par1', 'par2' => 'par2');
	
	var_dump($netwf->procedure("sp_procedure", $params));

	// Execute script1.sql script with parameters
	$params = array('@par1' => 'value1');
	
	var_dump($netwf->script("script1.sql", $params));
?>