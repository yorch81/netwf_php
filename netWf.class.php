<?php
/**
 * netWf 
 *
 * netWf netWf PHP Client
 *
 * Copyright 2017 Jorge Alberto Ponce Turrubiates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   netWf
 * @package    netWf
 * @copyright  Copyright 2017 Jorge Alberto Ponce Turrubiates
 * @license    http://www.apache.org/licenses/LICENSE-2.0
 * @version    1.0.0, 2017-10-20
 * @author     Jorge Alberto Ponce Turrubiates (the.yorch@gmail.com)
 */
class netWf
{

	/**
     * netWf Server URL
     * 
     * @var string $_url netWf Server URL
     *
     * @access private
     */
	private $_url;

	/**
     * netWf Server Key
     * 
     * @var string $_key netWf Server Key
     *
     * @access private
     */
	private $_key;

	/**
	 * CURL Resource
	 * 
	 * @var resource $_curl CURL
	 */
	private $_curl;

	/**
	 * Constructor of class
	 * 
	 * @param string $url    URL
	 * @param string $key    Private Key
	 */
	public function __construct($url, $key)
	{
		$this->_url = $url;
		$this->_key = $key;

		if (!extension_loaded('curl')) {
            die("CURL PHP Module not loaded !!!");
        }
	}

	/**
	 * Gets URL
	 * 
	 * @return string URL
	 */
	public function getUrl()
	{
		return $this->_url;
	}

	/**
	 * Gets Key
	 * 
	 * @return string Key
	 */
	public function getKey()
	{
		return $this->_key;
	}

	/**
	 * Init CURL Resource
	 * 
	 * @param  string $url URL
	 */
	private function curl_init($url)
	{
		$this->_curl = curl_init($url);

		curl_setopt($this->_curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2");
		curl_setopt($this->_curl, CURLOPT_HEADER, false);
		curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array("Accept-Language: es-es,en"));
		curl_setopt($this->_curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->_curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($this->_curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($this->_curl, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($this->_curl, CURLOPT_TIMEOUT, 60);
		curl_setopt($this->_curl, CURLOPT_AUTOREFERER, TRUE);
	}

	/**
	 * Close CURL Resource
	 */
	private function curl_close()
	{
		curl_close($this->_curl);
	}

	/**
	 * Gets Table Rows with limit
	 * 
	 * @param  string $tableName Table Name
	 * @param  int    $limit     Limit Rows
	 * @return array
	 */
	public function table($tableName, $limit)
	{
		$retValue = array();

		$url = $this->_url . '/api/table/' . $tableName . '/' . $limit . '?auth=' . $this->_key;

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
		curl_setopt($this->_curl, CURLOPT_POST, false); 

		$response = curl_exec ($this->_curl); 

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}

	/**
	 * Gets Row by Id
	 * 
	 * @param  string $tableName Table Name
	 * @param  string $fieldKey  Field Key
	 * @param  int 	  $key       Id Row
	 * @return array
	 */
	public function tableById($tableName, $fieldKey, $key)
	{
		$retValue = array();

		$url = $this->_url . '/api/tablebyid/' . $tableName . '/' . $fieldKey . '/' . $key . '?auth=' . $this->_key;

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
		curl_setopt($this->_curl, CURLOPT_POST, false); 

		$response = curl_exec ($this->_curl); 

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}

	/**
	 * Gets First Row of Table
	 * 
	 * @param  string $tableName Table Name
	 * @param  string $fieldKey  Field Key
	 * @param  string $where     Where Condition or * for not Where
	 * @return array
	 */
	public function first($tableName, $fieldKey, $where)
	{
		$retValue = array();

		$url = $this->_url . '/api/first/' . $tableName . '/' . $fieldKey . '/0/' . $where . '?auth=' . $this->_key;

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
		curl_setopt($this->_curl, CURLOPT_POST, false); 

		$response = curl_exec ($this->_curl); 

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}

	/**
	 * Gets Previous Row of Table
	 * 
	 * @param  string $tableName Table Name
	 * @param  string $fieldKey  Field Key
	 * @param  int    $key       Id Row
	 * @param  string $where     Where Condition or * for not Where
	 * @return array
	 */
	public function previous($tableName, $fieldKey, $key, $where)
	{
		$retValue = array();

		$url = $this->_url . '/api/previous/' . $tableName . '/' . $fieldKey . '/' . $key . '/' . $where . '?auth=' . $this->_key;

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
		curl_setopt($this->_curl, CURLOPT_POST, false); 

		$response = curl_exec ($this->_curl); 

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}

	/**
	 * Gets Next Row of Table
	 * 
	 * @param  string $tableName Table Name
	 * @param  string $fieldKey  Field Key
	 * @param  int    $key       Id Row
	 * @param  string $where     Where Condition or * for not Where
	 * @return array
	 */
	public function next($tableName, $fieldKey, $key, $where)
	{
		$retValue = array();

		$url = $this->_url . '/api/next/' . $tableName . '/' . $fieldKey . '/' . $key . '/' . $where . '?auth=' . $this->_key;

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
		curl_setopt($this->_curl, CURLOPT_POST, false); 

		$response = curl_exec ($this->_curl); 

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}

	/**
	 * Gets Last Row of Table
	 * 
	 * @param  string $tableName Table Name
	 * @param  string $fieldKey  Field Key
	 * @param  string $where     Where Condition or * for not Where
	 * @return array
	 */
	public function last($tableName, $fieldKey, $where)
	{
		$retValue = array();

		$url = $this->_url . '/api/last/' . $tableName . '/' . $fieldKey . '/0/' . $where . '?auth=' . $this->_key;

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
		curl_setopt($this->_curl, CURLOPT_POST, false); 

		$response = curl_exec ($this->_curl); 

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}

	/**
	 * Execute Stored Procedure from netWf
	 * 
	 * @param  string $procedure  Stored Procedure Name
	 * @param  array $parameters Array Parameters
	 * @return array
	 */
	public function procedure($procedure, $parameters)
	{	
		$retValue = array();

		$url = $this->_url . '/api/procedure/' . $procedure . '?auth=' . $this->_key;

		$jsonData = json_encode($parameters);

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
        curl_setopt($this->_curl, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $jsonData);                                                                    
		curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($jsonData))); 

		$response = curl_exec ($this->_curl);

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}

	/**
	 * Execute Remote SQL Script from netWf
	 * 
	 * @param  string $scriptName SQL Script Name
	 * @param  array $parameters Array Parameters
	 * @return array
	 */
	public function script($scriptName, $parameters)
	{	
		$retValue = array();

		$url = $this->_url . '/api/script/' . $scriptName . '?auth=' . $this->_key;

		$jsonData = json_encode($parameters);

		// Init CURL
		$this->curl_init($url);

		curl_setopt($this->_curl, CURLOPT_URL, $url);
        curl_setopt($this->_curl, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $jsonData);                                                                    
		curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($jsonData))); 

		$response = curl_exec ($this->_curl);

		// Close CURL
		$this->curl_close();

		if($response === false){
			return $retValue;
		}
		else{
		    $retValue = (array) json_decode($response, true);

		    return $retValue;
		}
	}
}
?>