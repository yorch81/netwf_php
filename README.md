# netWf 4 PHP #

## Description ##
netDb Web Framework PHP Client.

## Requirements ##
* [PHP 5.4.1 or higher](http://www.php.net/)
* [netWf](https://bitbucket.org/yorch81/netwf)
* [netWf on IIS](https://bitbucket.org/yorch81/netwf_iis)
* [netWf Server](https://bitbucket.org/yorch81/netsrv)

## Developer Documentation ##
Execute phpdoc -d netwf_php/

## Installation ##
Create file composer.json

~~~
{
    "require": {
    	"php": ">=5.4.0",
        "yorch/netwf" : "dev-master"
    }
}
~~~

## Example ##
~~~

$netwf = new netWf(URL, KEY);

// Get 5 Rows of Table
var_dump($netwf->table("table", 5));

// Get Row with id = 2
var_dump($netwf->tableById("table", "id", 2));

// Get First Row
var_dump($netwf->first("table", "id", "*"));

// Get Last Row
var_dump($netwf->last("table", "id", "*"));

// Get Previous Row of 3
var_dump($netwf->previous("table", "id", 3, "*"));

// Get Next Row of 3
var_dump($netwf->next("table", "id", 3, "*"));

// Execute sp_procedure
$params = array('par1' => 'par1', 'par2' => 'par2');

var_dump($netwf->procedure("sp_procedure", $params));

// Execute script1.sql script with parameters
$params = array('@par1' => 'value1');

var_dump($netwf->script("script1.sql", $params));

~~~

## References ##
https://es.wikipedia.org/wiki/Representational_State_Transfer

P.D. Let's go play !!!








